﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Project_Structure.DAL.Entities
{
    public class Project : BaseEntity
    {
        [MaxLength(200)]
        public string? Name { get; set; }
        [MaxLength(300)]
        public string? Description { get; set; }
        [DataType(DataType.Date)]
        public DateTime Deadline { get; set; }
        [DataType(DataType.Date)]
        public DateTime CreatedAt { get; set; }

        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "The field {0} must be greater than {1}.")]
        public int TeamId { get; set; }
        [JsonIgnore]
        public Team Team { get; set; }
        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "The field {0} must be greater than {1}.")]
        public int AuthorId { get; set; }
        [JsonIgnore]
        public User Author { get; set; }
        [JsonIgnore]
        public ICollection<Task> Tasks { get; set; }

        public Project()
        {
            Tasks = new List<Task>();
        }
    }
}
