﻿using Microsoft.EntityFrameworkCore;
using Project_Structure.DAL.Context;
using Project_Structure.DAL.Entities;
using Project_Structure.DAL.Interfaces;
using Project_Structure.DAL.Interfaces.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Project_Structure.DAL.Repositories
{
    public class TeamRepository : BaseRepository, IRepository<Team>
    {
        public TeamRepository(ProjectDbContext context)
            : base(context)
        {
        }

        public void Create(Team item)
        {
            item.Id = 0;
            _db.Teams.Add(item);
        }

        public void Delete(int id)
        {
            var toRemove = _db.Teams.SingleOrDefault(x => x.Id == id);
            if (toRemove is not null)
            {
                _db.Teams.Remove(toRemove);
            }
        }

        public IEnumerable<Team> Find(Func<Team, bool> predicate)
        {
            return _db.Teams.AsNoTracking().Where(predicate).ToList();
        }

        public Team Get(int id)
        {
            return _db.Teams.AsNoTracking().SingleOrDefault(x => x.Id == id);
        }

        public IEnumerable<Team> GetAll()
        {
            return _db.Teams.AsNoTracking().ToList();
        }

        public void Update(Team item)
        {
            var trackedTeam = _db.Teams.Find(item.Id);
            _db.Entry(trackedTeam).CurrentValues.SetValues(item);
        }
        public void Dispose()
        {
            _db.Dispose();
        }
    }
}
