﻿using Microsoft.EntityFrameworkCore;
using Project_Structure.DAL.Context;
using Project_Structure.DAL.Entities;
using Project_Structure.DAL.Interfaces;
using Project_Structure.DAL.Interfaces.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Project_Structure.DAL.Repositories
{
    public class ProjectRepository : BaseRepository, IRepository<Project>
    {
        public ProjectRepository(ProjectDbContext context)
            : base(context)
        {
        }

        public void Create(Project item)
        {
            item.Id = 0;
            _db.Projects.Add(item);
        }

        public void Delete(int id)
        {
            var toRemove = _db.Projects.SingleOrDefault(x => x.Id == id);
            if (toRemove is not null)
            {
                _db.Projects.Remove(toRemove);
            }
        }

        public IEnumerable<Project> Find(Func<Project, bool> predicate)
        {
            return _db.Projects.AsNoTracking().Where(predicate).ToList();
        }

        public Project Get(int id)
        {
            return _db.Projects.AsNoTracking().SingleOrDefault(x => x.Id == id);
        }

        public IEnumerable<Project> GetAll()
        {
            return _db.Projects.AsNoTracking().ToList();
        }

        public void Update(Project item)
        {
            var trackedProject = _db.Projects.Find(item.Id);
            _db.Entry(trackedProject).CurrentValues.SetValues(item);
        }
        public void Dispose()
        {
            _db.Dispose();
        }
    }
}
