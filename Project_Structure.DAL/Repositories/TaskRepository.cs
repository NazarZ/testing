﻿using Microsoft.EntityFrameworkCore;
using Project_Structure.DAL.Context;
using Project_Structure.DAL.Entities;
using Project_Structure.DAL.Interfaces;
using Project_Structure.DAL.Interfaces.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Project_Structure.DAL.Repositories
{
    public class TaskRepository : BaseRepository, IRepository<Task>
    {
        public TaskRepository(ProjectDbContext context)
            : base(context)
        {
        }

        public void Create(Task item)
        {
            item.Id = 0;
            _db.Tasks.Add(item);
        }

        public void Delete(int id)
        {
            var toRemove = _db.Tasks.SingleOrDefault(x => x.Id == id);
            if (toRemove is not null)
            {
                _db.Tasks.Remove(toRemove);
            }
            else
            {
                throw new ArgumentException("Task does not exist", "404");
            }
        }

        public IEnumerable<Task> Find(Func<Task, bool> predicate)
        {
            return _db.Tasks.AsNoTracking().Where(predicate).ToList();
        }

        public Task Get(int id)
        {
            return _db.Tasks.AsNoTracking().SingleOrDefault(x => x.Id == id);
        }

        public IEnumerable<Task> GetAll()
        {
            return _db.Tasks.AsNoTracking().ToList();
        }

        public void Update(Task item)
        {
            var trackedTask = _db.Tasks.Find(item.Id);
            _db.Entry(trackedTask).CurrentValues.SetValues(item);
        }
        public void Dispose()
        {
            _db.Dispose();
        }
    }
}
