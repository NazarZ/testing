﻿using Project_Structure.BLL.DTOs;
using System;

namespace Project_Structure.Console_UI
{
    class Program
    {
        static Query _query;
        static void Main(string[] args)
        {
            var i = new UserDTO()
            {
                FirstName = "New UserName ooooooooooooooooooooooooooo",
                LastName = "New User Last Name",
                BirthDay = DateTime.Now.AddYears(-20),
                RegisteredAt = DateTime.Now
            };
            _query = new();
            MainMenu();
            Console.ReadKey();
        }

        public async static void MainMenu()
        {
            Console.WriteLine("WELCOME TO THE MAIN MENU");
            Console.WriteLine("\t1 - Вивести кількість завдань у проекті конкретного користувача (по id)");
            Console.WriteLine("\t2 - Вивести список тасків, призначених для конкретного користувача (по id)");
            Console.WriteLine("\t3 - Вивести список тасків, які виконані в поточному (2021) році для конкретного користувача (по id)");
            Console.WriteLine("\t4 - Вивести список з колекції команд, учасники яких старші 10 років, відсортованих за датою реєстрації користувача за спаданням, а також згрупованих по командах.");
            Console.WriteLine("\t5 - Вивести список користувачів за алфавітом  алфавітом first_name (по зростанню) з відсортованими завданнями по довжині name (за спаданням).");
            Console.WriteLine("\t6 - Вивести структуру");
            Console.WriteLine("\t7 - Вивести структуру");
            Console.WriteLine("To quit from the program - enter 0\n");
            int userChoice = Convert.ToInt32(Console.ReadLine());
            switch (userChoice)
            {
                case 0:
                    Environment.Exit(0);
                    break;
                case 1:
                    GetTasksAmountByUserId();
                    break;
                case 2:
                    GetTasksAmounForConcreteUser();
                    break;
                case 3:
                    GetFinishedTasksByUser();
                    break;
                case 4:
                    GetTeams();
                    break;
                case 5:
                    GetUserOrderByNameWithTask();
                    break;
                case 6:
                    GetTask6();
                    break;
                case 7:
                    //GetTask6();
                    break;
                default:
                    MainMenu();
                    break;
            }
        }

        public static void GetTasksAmountByUserId()
        {
            try
            {
                Console.WriteLine("\nEnter a user Id:");
                var userId = Convert.ToInt32(Console.ReadLine());
                _query.GetCountTasksUserInProjects(userId);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                MainMenu();
            }
        }

        public static void GetTasksAmounForConcreteUser()
        {
            try
            {
                Console.WriteLine("\nEnter a user Id:");
                var userId = Convert.ToInt32(Console.ReadLine());
                _query.GetTasksByUserId(userId);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                MainMenu();
            }
        }

        public static void GetFinishedTasksByUser()
        {
            try
            {
                Console.WriteLine("\nEnter a user Id:");
                var userId = Convert.ToInt32(Console.ReadLine());
                _query.GetFinishedTaskByUserId(userId);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                MainMenu();
            }
        }

        public static void GetTeams()
        {
            try
            {
                _query.GetTeams();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                MainMenu();
            }
        }

        public static void GetUserOrderByNameWithTask()
        {
            try
            {
                _query.GetUserOrderByNameWithTask();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                MainMenu();
            }
        }

        public static void GetTask6()
        {
            try
            {
                Console.WriteLine("\nEnter a user Id:");
                var userId = Convert.ToInt32(Console.ReadLine());
                _query.GetTask6(userId);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                MainMenu();
            }
        }

        public static void GetTask7()
        {
            try
            {
                //_query.GetTask7();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                MainMenu();
            }
        }
    }
}
