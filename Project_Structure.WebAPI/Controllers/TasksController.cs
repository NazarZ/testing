﻿using Microsoft.AspNetCore.Mvc;
using Project_Structure.BLL.DTOs;
using Project_Structure.BLL.Interfaces;
using System;
using System.Collections.Generic;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Project_Structure.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TasksController : ControllerBase
    {
        ITaskService _taskService;
        public TasksController(ITaskService service)
        {
            _taskService = service;
        }
        // GET: api/Projects
        [HttpGet]
        public ActionResult<IEnumerable<TaskDTO>> Get()
        {
            Response.ContentType = "application/json";
            return Ok(_taskService.GetAllTasks());
        }

        // GET: api/Tasks/id
        [HttpGet("{id}")]
        public ActionResult Get(string id)
        {
            try
            {
                Response.ContentType = "application/json";
                var result = _taskService.GetTaskById(Convert.ToInt32(id));
                return result is not null ? Ok(result) : NotFound();
            }
            catch (ArgumentException e)
            {
                if (e.ParamName == "400")
                {
                    return BadRequest(e.Message);
                }

                if (e.ParamName == "404")
                {
                    return NotFound(e.Message);
                }

                return NotFound(e.Message);
            }
        }
        // POST: api/Tasks
        //create
        [HttpPost]
        public ActionResult<TaskDTO> Post([FromBody] TaskDTO task)
        {
            try
            {
                if (task is not null)
                {
                    var createdTask = _taskService.CreateTask(task);
                    Response.ContentType = "application/json";
                    return Created("", createdTask);
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (ArgumentException e)
            {
                if (e.ParamName == "400")
                {
                    return BadRequest(e.Message);
                }

                if (e.ParamName == "404")
                {
                    return NotFound(e.Message);
                }

                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        // PUT: api/Tasks
        //Update
        [HttpPut]
        public ActionResult<TaskDTO> Put([FromBody] TaskDTO task)
        {
            try
            {
                if (task is not null)
                {
                    _taskService.UpdateTask(task);
                    Response.ContentType = "application/json";
                    return Created("", task);
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (ArgumentException e)
            {
                if (e.ParamName == "400")
                {
                    return BadRequest(e.Message);
                }

                if (e.ParamName == "404")
                {
                    return NotFound(e.Message);
                }

                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }



        // DELETE: api/Tasks/id
        [HttpDelete("{id}")]
        public ActionResult Delete(string id)
        {
            try
            {
                Response.ContentType = "application/json";
                _taskService.DeleteTask(Convert.ToInt32(id));
                return NoContent();
            }
            catch (ArgumentException e)
            {
                if (e.ParamName == "400")
                {
                    return BadRequest(e.Message);
                }

                if (e.ParamName == "404")
                {
                    return NotFound(e.Message);
                }

                return NotFound(e.Message);
            }
            catch (InvalidOperationException e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}
