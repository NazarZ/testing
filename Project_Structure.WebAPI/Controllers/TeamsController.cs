﻿using Microsoft.AspNetCore.Mvc;
using Project_Structure.BLL.DTOs;
using Project_Structure.BLL.Interfaces;
using System;
using System.Collections.Generic;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Team_Structure.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TeamsController : ControllerBase
    {
        private readonly ITeamService _teamService;
        public TeamsController(ITeamService service)
        {
            _teamService = service;
        }
        // GET: api/Teams
        [HttpGet]
        public ActionResult<IEnumerable<TeamDTO>> Get()
        {
            Response.ContentType = "application/json";
            return Ok(_teamService.GetAllTeams());
        }

        // GET: api/Teams/id
        [HttpGet("{id}")]
        public ActionResult Get(string id)
        {
            try
            {
                Response.ContentType = "application/json";
                var result = _teamService.GetTeamById(Convert.ToInt32(id));
                return result is not null ? Ok(result) : NotFound();
            }
            catch (ArgumentException e)
            {
                if (e.ParamName == "400")
                {
                    return BadRequest(e.Message);
                }

                if (e.ParamName == "404")
                {
                    return NotFound(e.Message);
                }

                return NotFound(e.Message);
            }
        }
        // POST: api/Teams
        //create
        [HttpPost]
        public ActionResult<TeamDTO> Post([FromBody] TeamDTO team)
        {
            try
            {
                if (team is not null)
                {
                    var created = _teamService.CreateTeam(team);
                    Response.ContentType = "application/json";
                    return Created("", created);
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (ArgumentException e)
            {
                if (e.ParamName == "400")
                {
                    return BadRequest(e.Message);
                }

                if (e.ParamName == "404")
                {
                    return NotFound(e.Message);
                }

                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        // PUT: api/Teams
        //Update
        [HttpPut]
        public ActionResult<TeamDTO> Put([FromBody] TeamDTO team)
        {
            try
            {
                if (team is not null)
                {
                    _teamService.UpdateTeam(team);
                    Response.ContentType = "application/json";
                    return Created("", team);
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (ArgumentException e)
            {
                if (e.ParamName == "400")
                {
                    return BadRequest(e.Message);
                }

                if (e.ParamName == "404")
                {
                    return NotFound(e.Message);
                }

                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }



        // DELETE: api/Vehicle/id
        [HttpDelete("{id}")]
        public ActionResult Delete(string id)
        {
            try
            {
                Response.ContentType = "application/json";
                _teamService.DeleteTeam(Convert.ToInt32(id));
                return NoContent();
            }
            catch (ArgumentException e)
            {
                if (e.ParamName == "400")
                {
                    return BadRequest(e.Message);
                }

                if (e.ParamName == "404")
                {
                    return NotFound(e.Message);
                }

                return NotFound(e.Message);
            }
            catch (InvalidOperationException e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}
