﻿using Microsoft.AspNetCore.Mvc;
using Project_Structure.BLL.Interfaces;
using Project_Structure.BLL.Models;
using System.Collections.Generic;

namespace Project_Structure.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class QueriesController : ControllerBase
    {
        private readonly IQueryService _queryService;
        public QueriesController(IQueryService service)
        {
            _queryService = service;
        }
        // GET: api/Projects
        [HttpGet("GetCountTasksUserInProjects/{id}")]
        public ActionResult<Dictionary<ProjectModel, int>> GetCountTasksUserInProjects(string id)
        {
            int iD;
            if (int.TryParse(id, out iD))
            {
                return Ok(_queryService.GetCountTasksUserInProjects(iD));
            }
            else
            {
                return BadRequest("Invalid ID");
            }
        }
        [HttpGet("GetTasksByUserId/{id}")]
        public ActionResult<List<TaskModel>> GetTasksByUserId(string id)
        {
            int iD;
            if (int.TryParse(id, out iD))
            {
                var response = _queryService.GetTasksByUserId(iD);
                return Ok(response);
            }
            else
            {
                return BadRequest("Invalid ID");
            }
        }
        [HttpGet("GetFinishedTaskByUserId/{id}")]
        public ActionResult<List<FinishedTaskModel>> GetFinishedTaskByUserId(string id)
        {
            int iD;
            if (int.TryParse(id, out iD))
            {
                return Ok(_queryService.GetFinishedTaskByUserId(iD));
            }
            else
            {
                return BadRequest("Invalid ID");
            }


        }
        [HttpGet("GetTeams")]
        public ActionResult<List<TeamUsers>> GetTeams()
        {
            return Ok(_queryService.GetTeams());
        }
        [HttpGet("GetUserOrderByNameWithTask")]
        public ActionResult<List<UserWithTaskModel>> GetUserOrderByNameWithTask()
        {
            return Ok(_queryService.GetUserOrderByNameWithTask());
        }
        [HttpGet("GetTask6/{id}")]
        public ActionResult<Task6Model> GetTask6(string id)
        {
            int iD;
            if (int.TryParse(id, out iD))
            {
                return Ok(_queryService.GetTask6(iD));
            }
            else
            {
                return BadRequest("Invalid ID");
            }
        }




    }
}
