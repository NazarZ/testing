﻿using AutoMapper;
using Project_Structure.BLL.DTOs;
using Project_Structure.BLL.Interfaces;
using Project_Structure.BLL.Services.Abstract;
using Project_Structure.DAL.Entities;
using Project_Structure.DAL.Interfaces;
using System.Collections.Generic;

namespace Project_Structure.BLL.Services
{
    public class UserService : BaseService, IUserService
    {
        public UserService(IUnitOfWork context, IMapper mapper)
            : base(context, mapper)
        {
        }
        public UserDTO CreateUser(UserDTO user)
        {
            var userEntity = _mapper.Map<User>(user);
            _context.Users.Create(userEntity);
            _context.Save();
            return _mapper.Map<UserDTO>(userEntity);
        }

        public void DeleteUser(int idItem)
        {
            _context.Users.Delete(idItem);
            _context.Save();
        }

        public IEnumerable<UserDTO> GetAllUsers()
        {
            return _mapper.Map<IEnumerable<UserDTO>>(_context.Users.GetAll());
        }

        public UserDTO GetUserById(int idItem)
        {
            return _mapper.Map<UserDTO>(_context.Users.Get(idItem));
        }

        public UserDTO UpdateUser(UserDTO item)
        {
            var userEntity = _mapper.Map<User>(item);
            _context.Users.Update(userEntity);
            _context.Save();
            return _mapper.Map<UserDTO>(userEntity);
        }
    }
}
