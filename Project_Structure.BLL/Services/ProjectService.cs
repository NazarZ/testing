﻿using AutoMapper;
using Project_Structure.BLL.DTOs;
using Project_Structure.BLL.Interfaces;
using Project_Structure.BLL.Services.Abstract;
using Project_Structure.DAL.Entities;
using Project_Structure.DAL.Interfaces;
using System.Collections.Generic;

namespace Project_Structure.BLL.Services
{
    public class ProjectService : BaseService, IProjectService
    {
        public ProjectService(IUnitOfWork context, IMapper mapper)
            : base(context, mapper)
        {
        }
        public ProjectDTO CreateProject(ProjectDTO project)
        {
            var projectEntity = _mapper.Map<Project>(project);
            _context.Projects.Create(projectEntity);
            _context.Save();
            return _mapper.Map<ProjectDTO>(projectEntity);
        }

        public void DeleteProject(int idProject)
        {
            _context.Projects.Delete(idProject);
            _context.Save();
        }

        public void Dispose()
        {

        }

        public IEnumerable<ProjectDTO> GetAllProjects()
        {
            return _mapper.Map<List<ProjectDTO>>(_context.Projects.GetAll());
        }

        public ProjectDTO GetProjectById(int id)
        {
            return _mapper.Map<ProjectDTO>(_context.Projects.Get(id));
        }

        public ProjectDTO UpdateProject(ProjectDTO project)
        {
            var projectEntity = _mapper.Map<Project>(project);
            _context.Projects.Update(projectEntity);
            _context.Save();
            return _mapper.Map<ProjectDTO>(projectEntity);
        }
    }
}
