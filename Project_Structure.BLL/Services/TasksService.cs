﻿using AutoMapper;
using Project_Structure.BLL.DTOs;
using Project_Structure.BLL.Interfaces;
using Project_Structure.BLL.Services.Abstract;
using Project_Structure.DAL.Entities;
using Project_Structure.DAL.Interfaces;
using System;
using System.Collections.Generic;

namespace Project_Structure.BLL.Services
{
    public class TasksService : BaseService, ITaskService, IDisposable
    {
        public TasksService(IUnitOfWork context, IMapper mapper)
            : base(context, mapper)
        {

        }
        public void Dispose()
        {
            _context.Dispose();
        }
        public TaskDTO CreateTask(TaskDTO task)
        {
            task.Id = 0;
            var taskEntity = _mapper.Map<Task>(task);
            _context.Tasks.Create(taskEntity);
            _context.Save();
            return _mapper.Map<TaskDTO>(taskEntity);
        }

        public void DeleteTask(int idTask)
        {
            _context.Tasks.Delete(idTask);
            _context.Save();
        }

        public IEnumerable<TaskDTO> GetAllTasks()
        {
            return _mapper.Map<List<TaskDTO>>(_context.Tasks.GetAll());
        }

        public TaskDTO GetTaskById(int id)
        {
            return _mapper.Map<TaskDTO>(_context.Tasks.Get(id));
        }

        public TaskDTO UpdateTask(TaskDTO task)
        {
            var taskEntity = _mapper.Map<Task>(task);
            _context.Tasks.Update(taskEntity);
            _context.Save();
            return _mapper.Map<TaskDTO>(taskEntity);
        }
    }
}
