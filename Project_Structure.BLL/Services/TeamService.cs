﻿using AutoMapper;
using Project_Structure.BLL.DTOs;
using Project_Structure.BLL.Interfaces;
using Project_Structure.BLL.Services.Abstract;
using Project_Structure.DAL.Entities;
using Project_Structure.DAL.Interfaces;
using System.Collections.Generic;

namespace Project_Structure.BLL.Services
{
    public class TeamService : BaseService, ITeamService
    {
        public TeamService(IUnitOfWork context, IMapper mapper)
            : base(context, mapper)
        {
        }
        public TeamDTO CreateTeam(TeamDTO team)
        {
            var teamEntity = _mapper.Map<Team>(team);
            _context.Teams.Create(teamEntity);
            _context.Save();
            return _mapper.Map<TeamDTO>(teamEntity);
        }

        public void DeleteTeam(int idTask)
        {
            _context.Teams.Delete(idTask);
            _context.Save();
        }

        public IEnumerable<TeamDTO> GetAllTeams()
        {

            return _mapper.Map<IEnumerable<TeamDTO>>(_context.Teams.GetAll());
        }

        public TeamDTO GetTeamById(int id)
        {
            return _mapper.Map<TeamDTO>(_context.Teams.Get(id));
        }

        public TeamDTO UpdateTeam(TeamDTO team)
        {
            var teamEntity = _mapper.Map<Team>(team);
            _context.Teams.Update(teamEntity);
            _context.Save();
            return _mapper.Map<TeamDTO>(teamEntity);
        }
    }
}
