﻿using Project_Structure.BLL.DTOs;
using System.Collections.Generic;

namespace Project_Structure.BLL.Interfaces
{
    public interface ITeamService
    {
        public IEnumerable<TeamDTO> GetAllTeams();
        public TeamDTO GetTeamById(int id);
        public TeamDTO CreateTeam(TeamDTO team);
        public TeamDTO UpdateTeam(TeamDTO team);
        public void DeleteTeam(int idTask);
    }
}
