﻿using Project_Structure.BLL.DTOs;
using System.Collections.Generic;

namespace Project_Structure.BLL.Interfaces
{
    public interface IUserService
    {
        public IEnumerable<UserDTO> GetAllUsers();
        public UserDTO GetUserById(int id);
        public UserDTO CreateUser(UserDTO user);
        public UserDTO UpdateUser(UserDTO user);
        public void DeleteUser(int idTask);
    }
}
