﻿using Project_Structure.BLL.Models;
using System.Collections.Generic;

namespace Project_Structure.BLL.Interfaces
{
    public interface IQueryService
    {
        /// <summary>
        ///1. Отримати кількість тасків у проекті конкретного користувача (по id) (словник, де key буде проект, а value кількість тасків).
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public Dictionary<ProjectModel, int> GetCountTasksUserInProjects(int userId);
        /// <summary>
        /// 2. Отримати список тасків, призначених для конкретного користувача (по id), де name таска <45 символів (колекція з тасків).
        /// </summary>
        /// <param name="userId"></param>
        public List<TaskModel> GetTasksByUserId(int userId);
        /// <summary>
        /// 3. Отримати список(id, name) з колекції тасків, які виконані(finished) в поточному(2021) році
        ///для конкретного користувача (по id).
        /// </summary>
        /// <param name="userId"></param>
        public List<FinishedTaskModel> GetFinishedTaskByUserId(int userId);
        /// <summary>
        ///4. Отримати список (id, ім'я команди і список користувачів) з колекції команд, учасники яких старші 10 років,
        ///відсортованих за датою реєстрації користувача за спаданням, а також згрупованих по командах.
        ///P.S -в цьому запиті допускається перевірка лише року народження користувача, без прив'язки до місяця/дня/часу народження.
        /// </summary>
        /// <returns></returns>
        public List<TeamUsers> GetTeams();
        /// <summary>
        ///5. Отримати список користувачів за алфавітом first_name (по зростанню) з відсортованими tasks
        ///по довжині name (за спаданням).
        /// </summary>
        public List<UserWithTaskModel> GetUserOrderByNameWithTask();
        /// <summary>
        ///6. Отримати наступну структуру (передати Id користувача в параметри):
        ///  User
        /// Останній проект користувача(за датою створення)
        /// Загальна кількість тасків під останнім проектом
        /// Загальна кількість незавершених або скасованих тасків для користувача
        /// Найтриваліший таск користувача за датою(найраніше створений - найпізніше закінчений)
        /// P.S. - в даному випадку, статус таска не має значення, фільтруємо тільки за датою.
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public Task6Model GetTask6(int userId);
    }
}
