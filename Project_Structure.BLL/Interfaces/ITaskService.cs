﻿using Project_Structure.BLL.DTOs;
using System.Collections.Generic;

namespace Project_Structure.BLL.Interfaces
{
    public interface ITaskService
    {
        public IEnumerable<TaskDTO> GetAllTasks();
        public TaskDTO GetTaskById(int id);
        public TaskDTO CreateTask(TaskDTO task);
        public TaskDTO UpdateTask(TaskDTO task);
        public void DeleteTask(int idTask);
    }
}
