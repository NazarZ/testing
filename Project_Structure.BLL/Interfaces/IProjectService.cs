﻿using Project_Structure.BLL.DTOs;
using System;
using System.Collections.Generic;

namespace Project_Structure.BLL.Interfaces
{
    public interface IProjectService : IDisposable
    {
        public IEnumerable<ProjectDTO> GetAllProjects();
        public ProjectDTO GetProjectById(int id);
        public ProjectDTO CreateProject(ProjectDTO project);
        public ProjectDTO UpdateProject(ProjectDTO project);
        public void DeleteProject(int idProject);
    }
}
