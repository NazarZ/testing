﻿using AutoMapper;
using FakeItEasy;
using Project_Structure.BLL.DTOs;
using Project_Structure.BLL.Services;
using Project_Structure.DAL.Entities;
using Project_Structure.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Xunit;

namespace Project_Structure.BLL.Tests
{
    public class UserServiceTests : IClassFixture<ObjectFactoryFixture>
    {
        private UserService _userService;
        private IUnitOfWork fakeContext;
        private IMapper fakeMapper;
        public UserServiceTests(ObjectFactoryFixture fixture)
        {
            _userService = new(fixture.CreateUnitOfWork(), fixture.Mapper);
        }
        [Fact]
        public void GetUserServiceIsNotNull()
        {
            Assert.NotNull(_userService);
        }
        [Fact]
        public void CreatedNewUser_Then_CalledCreateAndSaveMethodsOne()
        {
            _userService = CrateFakeUserService();

            var fakeUser = A.Fake<UserDTO>();

            var user = _userService.CreateUser(fakeUser);

            A.CallTo(() => fakeContext.Users.Create(A<User>._)).MustHaveHappenedOnceExactly();
            A.CallTo(() => fakeContext.Save()).MustHaveHappenedOnceExactly();
        }

        [Fact]
        public void UpdateddUser_Then_CalledUddateAndSaveMethodsOne()
        {
            _userService = CrateFakeUserService();
            var fakeUser = A.Fake<UserDTO>();

            var user = _userService.UpdateUser(fakeUser);

            A.CallTo(() => fakeMapper.Map<User>(A<UserDTO>._)).MustHaveHappenedOnceExactly();
            A.CallTo(() => fakeContext.Users.Update(A<User>._)).MustHaveHappenedOnceExactly();
            A.CallTo(() => fakeContext.Save()).MustHaveHappenedOnceExactly();
        }

        [Fact]
        public void DeletedUser_Then_CalledDeletedAndSaveMethodsOne()
        {
            _userService = CrateFakeUserService();
            ClearRecordedCalls();
            var fakeId = 1;
            _userService.DeleteUser(fakeId);

            A.CallTo(() => fakeContext.Users.Delete(A<int>._)).MustHaveHappenedOnceExactly();
            A.CallTo(() => fakeContext.Save()).MustHaveHappenedOnceExactly();
        }

        [Fact]
        public void GetUserById_Then_CalledGetMethodOne()
        {
            _userService = CrateFakeUserService();
            ClearRecordedCalls();
            var fakeId = 1;

            _userService.GetUserById(fakeId);

            A.CallTo(() => fakeContext.Users.Get(A<int>._)).MustHaveHappenedOnceExactly();
        }

        [Fact]
        public void CreateNewUser_Than_DBContainsNewUser()
        {
            var user = CreateUserDTO();

            var cratedUser = _userService.CreateUser(user);
            var listUser = _userService.GetAllUsers();

            Assert.Single(listUser, cratedUser);
        }


        [Theory]
        [InlineData("New UserName", "New User Last Name 0000000", "newemail@gmail.com")]
        [InlineData("New UserName 00000000000000000", "New User Last Name", "newemail@gmail.com")]
        [InlineData("New UserName", "New User Last Name", "newemaili.com")]
        public void CreateNewUser_WhileInvalidProperty_Than_DoesNotPassValidation(string firstName, string lastName, string email)
        {
            var user = new UserDTO()
            {
                FirstName = firstName,
                LastName = lastName,
                Email = email,
                BirthDay = DateTime.Now.AddYears(-20),
                RegisteredAt = DateTime.Now
            };
            var results = new List<ValidationResult>();
            var context = new System.ComponentModel.DataAnnotations.ValidationContext(user);
            Assert.False(Validator.TryValidateObject(user, context, results, true));
        }
        [Fact]
        public void DeleteUser_Than_DbNotContainsNewUser()
        {
            var user = CreateUserDTO();
            var cratedUser = _userService.CreateUser(user);

            _userService.DeleteUser(cratedUser.Id);
            var listUser = _userService.GetAllUsers();

            Assert.Empty(listUser);
        }
        [Fact]
        public void DeleteUse_When_UserDoesNotExist_Than_ArgumentExeption()
        {
            var user = CreateUserDTO();
            var cratedUser = _userService.CreateUser(user);

            _userService.DeleteUser(cratedUser.Id);
            Assert.Throws<ArgumentException>(() => _userService.DeleteUser(cratedUser.Id));
        }
        [Fact]
        public void AddUserToTeam_Than_DBContainsUserWithTeam()
        {
            var user = CreateUserDTO();

            var cratedUser = _userService.CreateUser(user);
            cratedUser.TeamId = 1;
            var updated = _userService.UpdateUser(cratedUser);

            var listUser = _userService.GetAllUsers();


            Assert.Single(listUser, cratedUser);
            Assert.Equal(updated.TeamId, listUser.Single(x => x.Id == updated.Id).TeamId);
        }


        private UserDTO CreateUserDTO()
        {
            return new()
            {
                FirstName = "New UserName",
                LastName = "New User Last Name",
                BirthDay = DateTime.Now.AddYears(-20),
                RegisteredAt = DateTime.Now
            };
        }


        private void ClearRecordedCalls()
        {
            Fake.ClearRecordedCalls(fakeContext);
            Fake.ClearRecordedCalls(fakeMapper);
        }
        private UserService CrateFakeUserService()
        {
            fakeContext = A.Fake<IUnitOfWork>();
            fakeMapper = A.Fake<IMapper>();
            return new(fakeContext, fakeMapper);
        }
    }
}
